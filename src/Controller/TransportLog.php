<?php


namespace App\Controller;



use Psr\Log\LoggerInterface;
use Swift_Events_TransportExceptionEvent;

class TransportLog implements \Swift_Events_TransportExceptionListener
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Invoked as a TransportException is thrown in the Transport system.
     *
     * @param Swift_Events_TransportExceptionEvent $evt
     */
    public function exceptionThrown(Swift_Events_TransportExceptionEvent $evt)
    {
//        try {
//            $evt->getException();
//        }

        $this->logger->info($evt->getException()->getMessage());
        return false;
    }
}
