<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MailController extends AbstractController
{
    /**
     * @Route("/mail", name="mail")
     */
    public function index(\Swift_Mailer $mailer, LoggerInterface $logger)
    {
        try {
            $logger->notice('szit');
        } catch (\Swift_TransportException $exception) {

        }

//        try {
//            $message = (new \Swift_Message('Hello Email'))
//                ->setFrom('send@example.com')
//                ->setTo('lasekmiroslaw@gmail.com')
//                ->setBody(
//                        'test'
//                    ,
//                    'text/html'
//                )
//            ;
//            $mailer->send($message);
//        } catch (\Exception $e) {
////             dd($e);
//        }

        return $this->render('mail/index.html.twig', [
            'controller_name' => 'MailController',
        ]);
    }
}
